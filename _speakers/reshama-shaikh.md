---
name: Reshama Shaikh
talks:
---
Reshama Shaikh is a statistician/data scientist based in New York City.
Reshama is the Director of Data Umbrella and an organizer for NYC PyLadies.
She is also on the Contributing Teams for scikit-learn and PyMC. She was
awarded the [Community Leadership Award from NumFOCUS in
2019](https://reshamas.github.io/on-receiving-2019-community-leadership-award-from-numfocus/),
and is a [Python Software Foundation
Fellow](https://pyfound.blogspot.com/2022/07/announcing-python-software-foundation.html).

---
name: Peter Vidos
talks:
- "Let them explore! Building interactive, animated reports in Streamlit with ipyvizzu \u0026 a few lines of Python"
---
Peter is the CEO & Co-Founder of [Vizzu](https://vizzuhq.com).

His primary focus is understanding how Vizzu's innovative approach to data
visualization can be put to good use. Listening to people complaining about
their current hurdles with building charts and presenting them is his main
obsession, next to figuring out how to help data professionals utilize the
power of animation in dataviz.

Peter has been involved with digital product development for over 15 years.
Earlier products/projects he worked on cover mobile app testing, online
analytics, data visualization, decision support, e-learning, educational
administration & social. Still, building a selfie teleport just for fun is
what he likes to boast about when asked about previous experiences.

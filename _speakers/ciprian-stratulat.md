---
name: Ciprian Stratulat
talks:
- "How (and Why) to Chain Generators in Python"
---
 Ciprian is a software engineer and the CTO of Edlitera. As a programming
and machine learning instructor, Ciprian is a big believer in first building
an intuition about a new topic, and then mastering it through guided
deliberate practice. Before Edlitera, Ciprian worked as a Software Engineer
in finance, biotech, genomics and e-book publishing. Ciprian holds a degree
in Computer Science from Harvard University.

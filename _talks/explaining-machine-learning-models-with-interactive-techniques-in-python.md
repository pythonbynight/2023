---
duration: 25
presentation_url:
room:
slot:
speakers:
- Brayan Kai Mwanyumba
title: "Explaining Machine Learning Models with Interactive Techniques in Python"
type: talk
video_url:
---
Machine Learning is a powerful tool used across industries, but it's crucial
to ensure fairness and transparency in AI-driven decision-making. In this
talk, we will explore interactive explainability techniques in Python for
understanding and improving machine learning models.

We will then dive into popular model explainers like SHAP values, LIME,
Partial dependence plots, and ICE plots. Through real-world case studies,
uncover why a person may be denied a mortgage or have a high risk of a heart
attack based on vital health stats. By extracting key features and their
values, we reveal the factors influencing these predictions.

Finally, we’ll discuss the vulnerabilities and shortcomings of these methods
and discuss the road ahead.

---
duration: 15
presentation_url:
room:
slot:
speakers:
- Mario Munoz
title: "Python2Nite"
type: talk
video_url:
---
*Python2Nite with Sneaky Snake* is a late night talk show hosted by Sneaky
Snake McHosty. In this week's episode, McHosty goes back to the future of
hypermedia to talk about the resurgence of of an old technological paradigm
within the Python space. Could this be the RESTful zen that Python web
developers long for? Will he cover it from all... _angles_? Watch to see
his... _reaction_!

---
duration: 10
presentation_url:
room:
slot:
speakers:
- Lesley Cordero
title: "Microservice Observability with Python and OpenTelemetry"
type: talk
video_url:
---
In this talk, we'll focus on using the OpenTelemetry standard for Python
observability, especially Python applications within distributed systems.
We'll dive into OpenTelemetry concepts such as auto vs manual
instrumentation and the different types of telemetry data, e.g. distributed
tracing, metrics, and logging.

We'll discuss this topic through the lens of a platformized approach to
implementing observability, and how this approach enables organizations to
adopt OpenTelemetry. We'll define best practices, influenced by DevOps,
shared tooling, and the support needed to create robust, observable
applications.

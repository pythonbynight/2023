---
duration: 10
presentation_url:
room:
slot:
speakers:
- Bard Fetler
title: "Are Large Language Models Inherently Biased?"
type: talk
video_url:
---
ChatGPT, Google Bard, and other large language models are all the rage now,
and attracting a lot of attention.  But they are known to not always produce
__truthful__ or __accurate__ text.  Why is that?

We'll explore some ideas behind the models and data, and possible ways to
improve model behavior.

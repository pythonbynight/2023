---
layout: post
title: Announcing PyGotham 2023
date: 2023-03-04 00:00 -0500
---
We’re pleased to announce that PyGotham will take place online October 6th and
7th, 2023. The conference will feature two days of talks covering Python and the
Python community.

As we’ve previously announced, we postponed last year’s PyGotham Film Festival
event. We’re including the film festival this year as a new event called
PyGotham Movie Night, a limited event showcasing short films from the Python
community.

The call for proposals will run April 15th - May 15th. Details are available at
<https://cfp.pygotham.tv>; in short, we’re looking for both traditional talks and
short films on any topic that would be of interest to the Python community.

## Future Plans

PyGotham has been an online-only event since 2020. We aim to revive our
in-person format in 2024, and we’ll continue to adapt the conference format to
meet the needs of the community. To do so, we’ll need your help; if you’re
interested in contributing to PyGotham and its community in 2023 and 2024, read
on.

## Call for Volunteers

The PyGotham organizing team is looking for new contributors. If you’re
interested in volunteering, please reach out to <organizers@pygotham.org>.
Conference organizing is a rewarding way to give back to the community, and we’d
be thrilled to have you on board. Roles are available covering many different
organizing responsibilities and time commitments ranging from a few meetings to
a full conference planning cycle.

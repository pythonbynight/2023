---
title: CFP Help Session Office Hours
date: 2023-04-14 08:00:00 -0400
---

Are you interested in proposing a talk or short film to PyGotham TV 2023? Do you
need a bit of extra help or feedback to complete your proposal? We're here to
help! Join the PyGotham organizing team to learn what goes into a good
conference proposal and to get individualized feedback to make your CFP entry
the best that it can be. We'll be hosting two of these sessions this month:
Tuesday, April 25th at 6:00PM EDT and Saturday, April 29th at 1:00PM EDT. To
join, email <program@pygotham.org> and mention which session works best for you,
and we'll respond with a calendar invite and video call link.
